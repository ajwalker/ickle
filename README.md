
# Ickle test

## Infrastructure

The `infrastructure/` directory contains Terraform configuration for configuring a minimal Kubernetes cluster for deploying runner managers to.

## Deployments / Releases

The runner managers, and other Kubernetes resources, can be controlled with GitOps (using Flux, Helm Operator and SealedSecrets).

The `releases/` directory contains the resources to be deployed. We deploy different environments to different GCP projects.

Any secrets within the `releases/` directory first need to be encrypted with Sealed Secrets. To create the runner registration token, for example, we would encrypt a secret and then commit the encrypted secret to the git repository:

`$ kubectl create secret generic -n runner gitlab-runner --dry-run=client --from-literal=runner-registration-token=`**\_\_GITLAB_RUNNER_TOKEN\_\_**` --from-literal=runner-token="" -o yaml | kubeseal --controller-name=sealed-secrets --controller-namespace=adm -o yaml > runner-secret.yaml`