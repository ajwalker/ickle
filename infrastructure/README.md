# Runner Terraform

This repository contains the terraform used to provision and update
infrastructure and services used by runner managers.

It uses a TerraServices-structure and Terraform workspaces to support identical
cluster deployments within the same project, with shared "core" resources.

There are 3 entrypoints, each with their own remote state. The remote state is
passed along to the next entrypoint, so the order of provisioning is important.

## Entrypoints
- #### infrastructure/core
  Configures global GCP project information.

- #### infrastructure/kubernetes-cluster
  Configures [Google Kubernetes Enginer (GKE)](https://cloud.google.com/kubernetes-engine/)
  cluster.

- #### infrastructure/kubernetes-runner
  Installs and configures flux, helm-provider and access required for the runner.

# Usage

- Create a GCP Project
- Create a GCS bucket for the terraform remote state
- Create a service account key (.json), with the following roles:
  - Compute Network Admin
  - Kubernetes Engine Admin
  - Service Account User
  - Service Usage Admin
  - Storage Admin

- Tell Terraform where your service account key lives:

  `export GOOGLE_APPLICATION_CREDENTIALS=~/key.json`

- Provision:

  Use `terraform plan` / `terraform apply` for each entry-point you need to
  provision/update.