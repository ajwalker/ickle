provider "google" {
  project = var.project
  region  = var.region
}

provider "google-beta" {
  project = var.project
  region  = var.region
}

data "google_compute_network" "private_network" {
  name = "default"
}

resource "google_container_cluster" "default" {
  name     = "${var.prefix}-runner-cluster"
  location = var.location

  min_master_version = "latest"

  remove_default_node_pool = true
  initial_node_count       = 1

  network = data.google_compute_network.private_network.self_link
}

resource "google_container_node_pool" "np" {
  name       = "${var.prefix}-cpu4-mem8192-pool"
  cluster    = google_container_cluster.default.name
  node_count = var.node_count
  location   = var.location

  autoscaling {
    min_node_count = var.min_node_count
    max_node_count = var.max_node_count
  }

  node_config {
    machine_type = "custom-4-8192"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "${var.prefix}-gke-peering"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = data.google_compute_network.private_network.self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network = data.google_compute_network.private_network.self_link
  service = "servicenetworking.googleapis.com"

  reserved_peering_ranges = ["${google_compute_global_address.private_ip_address.name}"]
}

output "endpoint" {
  value = google_container_cluster.default.endpoint
}

output "cluster_ca_certificate" {
  value = "${base64decode(google_container_cluster.default.master_auth.0.cluster_ca_certificate)}"
}

output "private_network" {
  value = data.google_compute_network.private_network.self_link
}
