variable "project" {}
variable "region" {}
variable "location" {}
variable "prefix" {}
variable "node_count" {
  default = 1
}
variable "min_node_count" {
  default = 0
}
variable "max_node_count" {
  default = 1
}
