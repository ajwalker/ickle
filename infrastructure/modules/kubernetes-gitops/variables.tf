variable "endpoint" {}
variable "cluster_ca_certificate" {}
variable "repository" {
  description = "git repository for flux to periodically check for updates"
}
variable "path" {
  description = "git repository path for flux to periodically check for updates"
}
