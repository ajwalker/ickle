provider "kubernetes" {
  load_config_file = false

  host = var.endpoint

  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}


provider "helm" {
  kubernetes {
    load_config_file = false

    host                   = var.endpoint
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = var.cluster_ca_certificate
  }
}

data "google_client_config" "default" {
}

resource "kubernetes_namespace" "flux" {
  metadata {
    name = "flux"
  }
}

resource "helm_release" "helm-operator" {
  name       = "helm-operator"
  repository = "https://charts.fluxcd.io"
  chart      = "helm-operator"
  namespace  = "flux"

  set {
    name  = "helm.versions"
    value = "v3"
  }

  depends_on = [
    kubernetes_namespace.flux
  ]
}

resource "helm_release" "flux" {
  name       = "flux"
  repository = "https://charts.fluxcd.io"
  chart      = "flux"
  namespace  = "flux"

  set {
    name  = "git.url"
    value = var.repository
  }

  set {
    name  = "git.path"
    value = var.path
  }

  set {
    name  = "git.readonly"
    value = "true"
  }

  depends_on = [
    kubernetes_namespace.flux
  ]
}
