terraform {
  backend "gcs" {
    bucket = "ajwalker-ickle-terraform-state"
    prefix = "terraform/core/state"
  }
}

locals {
  project = "group-verify-df9383"

  # https://cloud.google.com/compute/docs/regions-zones#available
  region = "europe-west2"
}

provider "google" {
  project = local.project
  region  = local.region
}

resource "google_project_service" "cloudresourcemanager_service" {
  service            = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "compute_service" {
  service            = "compute.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "servicenetworking_service" {
  service            = "servicenetworking.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "container_service" {
  service            = "container.googleapis.com"
  disable_on_destroy = false
}

/*resource "google_project_service" "container_service" {
  service = "container.googleapis.com"
}

resource "google_project_service" "iam_service" {
  service = "iam.googleapis.com"
}

resource "google_project_service" "servicenetworking_service" {
  service = "servicenetworking.googleapis.com"
}*/

output "project" {
  value = local.project
}

output "region" {
  value = local.region
}
