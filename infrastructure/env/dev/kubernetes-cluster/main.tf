terraform {
  backend "gcs" {
    bucket = "ajwalker-ickle-terraform-state"
    prefix = "terraform/kubernetes-cluster/state"
  }
}

data "terraform_remote_state" "core" {
  backend = "gcs"

  config = {
    bucket = "ajwalker-ickle-terraform-state"
    prefix = "terraform/core/state"
  }
}

module "cluster" {
  source = "../../../modules/kubernetes-cluster"

  project  = data.terraform_remote_state.core.outputs.project
  region   = data.terraform_remote_state.core.outputs.region
  prefix   = terraform.workspace
  location = "europe-west2-a"
}

output "endpoint" {
  value = module.cluster.endpoint
}

output "private_network" {
  value = module.cluster.private_network
}

output "cluster_ca_certificate" {
  value = module.cluster.cluster_ca_certificate
}
