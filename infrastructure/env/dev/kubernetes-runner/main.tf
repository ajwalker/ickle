
terraform {
  backend "gcs" {
    bucket = "ajwalker-ickle-terraform-state"
    prefix = "terraform/kubernetes-runner/state"
  }
}

data "terraform_remote_state" "cluster" {
  backend   = "gcs"
  workspace = terraform.workspace
  config = {
    bucket = "ajwalker-ickle-terraform-state"
    prefix = "terraform/kubernetes-cluster/state"
  }
}

module "gitops" {
  source = "../../../modules/kubernetes-gitops"

  repository = "https://gitlab.com/ajwalker/ickle.git"
  path       = "releases/staging"

  endpoint               = data.terraform_remote_state.cluster.outputs.endpoint
  cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.cluster_ca_certificate
}
